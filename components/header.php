<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Site Title</title>
    <meta name="language" content="English">
    <meta name="author" content="Jay Mewada">
    <link rel="stylesheet" href="css/grid.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/responsive-992.css">
    <link rel="stylesheet" href="css/responsive-767.css">
    <link rel="stylesheet" href="css/responsive-576.css">
    <meta name="keywords" content="Site Title Here">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.lineicons.com/3.0/lineicons.css" rel="stylesheet">
</head>

<body class="site-body">

    <header class="site-header">
        <div class="container">

        </div>
    </header>


    <main class="site-main">